//
//  StoryboardIdentifiers.swift
//  Yibby
//
//  Created by Kishy Kumar on 6/29/16.
//  Copyright © 2016 MyComp. All rights reserved.
//

import UIKit

public enum StoryboardIdentifier: String {
    case Login = "LoginViewController"
    case Settings = "SettingsViewController"
}
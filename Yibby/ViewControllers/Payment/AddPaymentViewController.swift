//
//  AddPaymentViewController.swift
//  Yibby
//
//  Created by Kishy Kumar on 7/12/16.
//  Copyright © 2016 Yibby. All rights reserved.
//

import UIKit
import Crashlytics
import BButton
import Braintree
import Stripe

class AddPaymentViewController: BaseYibbyViewController, CardIOPaymentViewControllerDelegate {

    // MARK: - Properties
    
    @IBOutlet weak var cardFieldsViewOutlet: CardFieldsView!
    @IBOutlet weak var deleteCardButtonOutlet: YibbyButton1!
    @IBOutlet weak var saveCardButtonOutlet: UIBarButtonItem!
    @IBOutlet weak var cancelButtonOutlet: UIBarButtonItem!
    @IBOutlet weak var scanCardButtonOutlet: UIButton!
    
    // TODO: Payment Type Icon
//    var cardNumberField: BTUICardHint?
    
    // The STPAPIClient talks directly to Stripe to get the Token 
    // given a payment card.
    //
    // Whereas, the StripeBackendAdapter is a protocol to talk to
    // our backend (baasbox) to handle payments.
    
#if YIBBY_USE_STRIPE_PAYMENT_SERVICE

    var apiAdapter: StripeBackendAPIAdapter = StripeBackendAPI.sharedClient
    var cardToBeEdited: STPCard?

#elseif YIBBY_USE_BRAINTREE_PAYMENT_SERVICE

    var apiAdapter: BraintreeBackendAPIAdapter = BraintreeBackendAPI.sharedClient
    var cardToBeEdited: BTPaymentMethodNonce?
    
#endif
    
    var addDelegate : AddPaymentViewControllerDelegate?
    var editDelegate : EditPaymentViewControllerDelegate?
    var signupDelegate: SignupPaymentViewControllerDelegate?
    
    var isEditCard: Bool! = false   // implicitly unwrapped optional
    var isSignup: Bool! = false // implicitly unwrapped optional
    
    // MARK: - Actions
    @IBAction func deleteCardAction(sender: AnyObject) {
        
        if self.isSignup == true {
            
            // add this card
            saveCard()
            return;
            
        } else {
        
            // Raise an alert to confirm if the user actually wants to perform the action
            AlertUtil.displayChoiceAlert("Are you sure you want to delete the card?",
                                         message: "",
                                         completionActionString: InterfaceString.OK,
                                         completionBlock: { () -> Void in
                
                ActivityIndicatorUtil.enableActivityIndicator(self.view)
                
#if YIBBY_USE_STRIPE_PAYMENT_SERVICE

                self.editDelegate?.editPaymentViewController(self, didRemovePaymentMethod: self.cardToBeEdited!, completion: {(error: NSError?) -> Void in
                    
                    ActivityIndicatorUtil.disableActivityIndicator(self.view)
                    
                    if let error = error {
                        self.handleCardTokenError(error)
                    }
                })
        
#elseif YIBBY_USE_BRAINTREE_PAYMENT_SERVICE
        
                self.editDelegate?.editPaymentViewController(self, didRemovePaymentMethod: self.cardToBeEdited!, completion: {(error: NSError?) -> Void in
                    
                    ActivityIndicatorUtil.disableActivityIndicator(self.view)
                    
                    if let error = error {
                        self.handleCardTokenError(error)
                    }
                })
        
#endif
                
            })
        }
    }
    
    @IBAction func saveButtonAction(sender: AnyObject) {
        
        if isSignup == true {
            
            // skip this step
            self.signupDelegate?.signupPaymentViewControllerDidSkip(self)

            return;
        }
        
        saveCard()
    }
    
    @IBAction func cancelButtonAction(sender: AnyObject) {
        if (isEditCard == true) {
            self.editDelegate?.editPaymentViewControllerDidCancel(self)
        } else {
            self.addDelegate?.addPaymentViewControllerDidCancel(self)
        }
    }
    
    @IBAction func scanCardAction(sender: AnyObject) {
        
        // if camera is disabled, display alert.
        if (CardIOUtilities.canReadCardWithCamera() == false) {
            AlertUtil.displayAlert("Camera disabled.", message: "Please give Camera permission to Yibby.")
            return;
        }
        
        // display the cardIO view controller.
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC.modalPresentationStyle = .FormSheet
        cardIOVC.disableManualEntryButtons = true
        presentViewController(cardIOVC, animated: true, completion: nil)
    }
    
    // MARK: - CardIO Delegate Functions
    
    func userDidCancelPaymentViewController(paymentViewController: CardIOPaymentViewController!) {
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func userDidProvideCreditCardInfo(cardInfo: CardIOCreditCardInfo!, inPaymentViewController paymentViewController: CardIOPaymentViewController!) {
        
        self.cardFieldsViewOutlet.prefillCardInformation(cardInfo.cardNumber, month: Int(cardInfo.expiryMonth), year: Int(cardInfo.expiryYear), cvc: cardInfo.cvv)
        
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Setup Functions 
    
    func setupUI() {
        if (isEditCard == true) {
            
            deleteCardButtonOutlet.hidden = false
            deleteCardButtonOutlet.setType(BButtonType.Danger)
            
        } else if (isSignup == true) {
            self.navigationItem.hidesBackButton = true
            
            deleteCardButtonOutlet.hidden = false
            deleteCardButtonOutlet.setTitle(InterfaceString.Add, forState: .Normal)
            deleteCardButtonOutlet.color = UIColor.appDarkGreen1()
            
            saveCardButtonOutlet.title = InterfaceString.Skip
            
            // remove the cancel button
            self.navigationItem.leftBarButtonItems?.removeAll()
            
        } else {
            deleteCardButtonOutlet.hidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        CardIOUtilities.preload()
        
        if let card = cardToBeEdited {
            
            // cardParams.number will have the last 4 of the card
#if YIBBY_USE_STRIPE_PAYMENT_SERVICE
            self.cardFieldsViewOutlet.numberInputTextField.placeholder = "************" + card.last4()
#elseif YIBBY_USE_BRAINTREE_PAYMENT_SERVICE
            self.cardFieldsViewOutlet.numberInputTextField.placeholder = "************" + card.localizedDescription
#endif

        }
        
        setupUI()
    }

    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Helper functions
    
    func handleCardTokenError(error: NSError) {
        AlertUtil.displayAlert(error.localizedDescription, message: error.localizedFailureReason ?? "")
    }
    
    func isInputCardValid() -> Bool {
//        cardFieldsViewOutlet.numberInputTextField.
//        cardFieldsViewOutlet.monthTextField.isInputValid(cardFieldsViewOutlet.monthTextField.text!, partiallyValid: <#T##Bool#>)
        return true;
    }
    
    func saveCard() {
        
        if !isInputCardValid() {
            return;
        }
        
        ActivityIndicatorUtil.enableActivityIndicator(self.view)
        
        let number = cardFieldsViewOutlet.numberInputTextField.text
        let expMonth = cardFieldsViewOutlet.monthTextField.text
        let expYear = cardFieldsViewOutlet.yearTextField.text
        let cvc = cardFieldsViewOutlet.cvcTextField.text
        
        #if YIBBY_USE_STRIPE_PAYMENT_SERVICE
            
            let apiClient = StripePaymentService.sharedInstance().apiClient!
            
            apiClient.createTokenWithCard(cardParams, completion: {(token: STPToken?, tokenError: NSError?) -> Void in
                
                if let tokenError = tokenError {
                    ActivityIndicatorUtil.disableActivityIndicator(self.view)
                    self.handleCardTokenError(tokenError)
                }
                else {
                    //                var phone: String = self.rememberMePhoneCell.contents
                    //                var email: String = self.emailCell.contents
                    
                    // TODO: We save the zipcode
                    if self.isEditCard == true {
                        
                        self.editDelegate?.editPaymentViewController(self, didCreateNewToken: token!, completion: {(error: NSError?) -> Void in
                            ActivityIndicatorUtil.disableActivityIndicator(self.view)
                            if let error = error {
                                self.handleCardTokenError(error)
                            }
                        })
                    } else if self.isSignup == true {
                        self.signupDelegate?.signupPaymentViewController(self, didCreateToken: token!, completion: {(error: NSError?) -> Void in
                            ActivityIndicatorUtil.disableActivityIndicator(self.view)
                            if let error = error {
                                self.handleCardTokenError(error)
                            }
                        })
                    } else {
                        self.addDelegate?.addPaymentViewController(self, didCreateToken: token!, completion: {(error: NSError?) -> Void in
                            ActivityIndicatorUtil.disableActivityIndicator(self.view)
                            if let error = error {
                                self.handleCardTokenError(error)
                            }
                        })
                    }
                }
            })
        #elseif YIBBY_USE_BRAINTREE_PAYMENT_SERVICE
            
            let cardClient: BTCardClient = BTCardClient(APIClient: BraintreePaymentService.sharedInstance().apiClient!)
            
            let card: BTCard = BTCard(number: number!,
                                      expirationMonth: expMonth!,
                                      expirationYear: expYear!,
                                      cvv: cvc!)
            
            cardClient.tokenizeCard(card, completion: {(tokenized: BTCardNonce?, error: NSError?) -> Void in
                
                if let error = error {
                    ActivityIndicatorUtil.disableActivityIndicator(self.view)
                    self.handleCardTokenError(error)
                }
                else {
                    //                var phone: String = self.rememberMePhoneCell.contents
                    //                var email: String = self.emailCell.contents
                    
                    // TODO: We save the zipcode
                    if self.isEditCard == true {
                        
                        self.editDelegate?.editPaymentViewController(self, didCreateNewToken: tokenized!, completion: {(error: NSError?) -> Void in
                            ActivityIndicatorUtil.disableActivityIndicator(self.view)
                            if let error = error {
                                self.handleCardTokenError(error)
                            }
                        })
                    } else if self.isSignup == true {
                        self.signupDelegate?.signupPaymentViewController(self, didCreateNonce: tokenized!, completion: {(error: NSError?) -> Void in
                            ActivityIndicatorUtil.disableActivityIndicator(self.view)
                            if let error = error {
                                self.handleCardTokenError(error)
                            }
                        })
                    } else {
                        self.addDelegate?.addPaymentViewController(self, didCreateNonce: tokenized!, completion: {(error: NSError?) -> Void in
                            ActivityIndicatorUtil.disableActivityIndicator(self.view)
                            if let error = error {
                                self.handleCardTokenError(error)
                            }
                        })
                    }
                }
            })
            
        #endif
    }
    
}

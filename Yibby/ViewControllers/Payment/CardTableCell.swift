//
//  CardCell.swift
//  Yibby
//
//  Created by Kishy Kumar on 7/14/16.
//  Copyright © 2016 Yibby. All rights reserved.
//

import UIKit

class CardTableCell : UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var cardBrandImageViewOutlet: UIImageView!

    @IBOutlet weak var cardTextLabelOutlet: UILabel!
    
    // MARK: - Setup functions
    
//    func configure(ride: Ride) {
//        
//    }
}
